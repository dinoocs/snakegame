class Snake {
  PVector pos; // potition
  PVector vel; // velocity
  ArrayList<PVector> hist;
  int len;
  int moveX = 0;
  int moveY = 0;

  Snake() {
    pos = new PVector(0,100 );
    vel = new PVector();
    hist = new ArrayList<PVector>();
    len = 0;
  }

  void update() {
    hist.add(pos.copy());
    pos.x += vel.x*grid;
    pos.y += vel.y*grid;
    moveX = int(vel.x);
    moveY = int(vel.y);

    pos.x = (pos.x + width) % width;
    pos.y = (pos.y + height) % height;

    if (hist.size() > len) {
      hist.remove(0);
    }


    for (PVector p : hist) {
      if (p.x == pos.x && p.y == pos.y) {
        dead = true;
        if (len > highscore) highscore = len;
        file2.stop();
      file3.loop();
      }
    }
  }
  
 
    

  void eat() {
    
    if (pos.x == food.x && pos.y == food.y) {
      file1.play();
      len++;
      if (speed > 5) speed--;
      newFood();
      newObstacle();
      
    }
  }
    
    void obstacle() {
    
    if (pos.x == obstacle.x && pos.y == obstacle.y) {
       dead = true;
       if (len > highscore) highscore = len;
      // if (pos.x == food.x && pos.y == food.y);
      //newObstacle();
      file2.stop();
      file3.loop();
      
    }
    
  }

  void show() {
    noStroke();
    fill(29,181,18);
    image(img2, pos.x, pos.y, grid, grid );
    for (PVector p : hist) {
      rect(p.x, p.y,grid , grid);
    }
  }
}

void keyPressed() {
  if (keyCode == LEFT && snake.moveX != 1) {
    snake.vel.x = -1;
    snake.vel.y = 0;
  } else if (keyCode == RIGHT && snake.moveX != -1) {
    snake.vel.x = 1;
    snake.vel.y = 0;
  } else if (keyCode == UP && snake.moveY != 1) {
    snake.vel.y = -1;
    snake.vel.x = 0;
  } else if (keyCode == DOWN && snake.moveY != -1) {
    snake.vel.y = 1;
    snake.vel.x = 0;
  }
   else if ( key == 'p' ) {
    noLoop();
  }
  else if ( key == 'r' ) {
    loop();
  }
}
